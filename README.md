Material used for the two-day computing crash course for the course *AP3082 Computational Physics*

This is a simplified version of the week-long [*Casimir programming course*](https://gitlab.kwant-project.org/pc/casimir_programming_course)

The material is organized in jupyter notebooks. You can use the computing server at https://compphys.quantumtinkerer.tudelft.nl/ to run them,
following the instructions at the in the [Course Notes](https://gitlab.kwant-project.org/computational_physics_19/course_notes). You can manually
copy the crash course materials there by [following these instructions](https://gitlab.kwant-project.org/computational_physics_19/course_notes/blob/master/howtos/casimir_prog_course.md)

In this crash course, we will cover the following topics:

*Day 1*
- [Basic python programming](day1_morning)
- [Scientific programming with numpy/matplotlib/scipy](day1_afternoon)

*Day 2*
- [Basic shell](day2_morning)
- [Using git for version control](day2_afternoon)

When working on your project, you will also find the need to properly organize your code (this will be assessed, too). You can make use of the
following materials for self-study, and use the code written during the project as exercise material! Of course, you can also ask usfor advice in class during the course.

- [Structuring python code](structuring_python_code)